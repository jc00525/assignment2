import unittest
from assignment2 import *


class MyTestCase(unittest.TestCase):
    # Set up for running tests
    def setUp(self):
        self.widget = MyFrame
        self.data = name
        self.click = MyFrame.OnClicked
        self.file = wb

    # Test for checking the label
    def test_priceFilter_label(self):
        self.widget.btnPrice = label = "Price Filter"
        self.assertTrue("Price Filter", self.widget.btnPrice)

    # Test for checking the label
    def test_monthlyFilter_label(self):
        self.widget.btnReview = label = "Monthly Review"
        self.assertTrue("Monthly Review", self.widget.btnReview)

    # Test to check if data array is empty
    def test_emptyArray(self):
        self.data = []
        self.assertNotEqual([], self.data)
        self.assertEqual([], self.data)

    # Test to check if the click works with input
    def test_onClick(self):
        self.click = btn = "Price Filter"
        self.assertEqual("Price Filter", self.click)
        self.assertNotEqual("", self.click)

    # Test to check it the file is correct
    def test_file(self):
        self.file = 'listings_dec18.xlsx'
        self.assertTrue('listings_dec18.xlsx', self.file)


if __name__ == '__main__':
    unittest.main()
