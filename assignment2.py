import sys
import openpyxl
import wx


wb = openpyxl.load_workbook('listings_dec18.xlsx')
sheet = wb['listings_dec18']

ID = []
name = []
price = []
review = []
room = []
avail = []

for row in sheet.iter_rows():
    ID.append(row[0].value)
    name.append(row[4].value)
    price.append(row[60].value)
    review.append(row[95].value)
    room.append(row[52].value)
    avail.append(row[74].value)





class MyFrame(wx.Frame):
    def __init__(self, var):
        super().__init__(parent=None)
        panel = wx.Panel(self)
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.index = 0

        # Creates GUI elements
        # Title element with changed font size
        st = wx.StaticText(panel, label="Airb-n-People")
        font = st.GetFont()
        font.PointSize += 10
        font = font.Bold()
        st.SetFont(font)
        sizer.Add(st, 0, wx.ALL | wx.CENTER, 5)

        # Search element
        self.var = var
        self.text_ctrl = wx.TextCtrl(panel)
        sizer.Add(self.text_ctrl, 0, wx.ALL | wx.LEFT, 5)
        self.var = self.text_ctrl.GetValue()

        self.list = wx.ListCtrl(panel, 12, style = wx.LC_REPORT)
        self.list.InsertColumn(0, ID[0], width = 50)
        self.list.InsertColumn(1, name[0], width=100)
        self.list.InsertColumn(2, price[0]+" ($)", width=100)
        self.list.InsertColumn(3, review[0], width=150)
        self.list.InsertColumn(4, room[0], width=100)
        self.list.InsertColumn(5, avail[0], width=100)
        self.list.EnsureVisible(20)

        for i in range(len(ID)):

            self.list.InsertItem(self.index, str(ID[i]))
            self.list.SetItem(self.index, 1, str(name[i]))
            self.list.SetItem(self.index, 2, str(price[i]))
            self.list.SetItem(self.index, 3, str(review[i]))
            self.list.SetItem(self.index, 4, str(room[i]))
            self.list.SetItem(self.index, 5, str(avail[i]))
            self.index += 1

        self.list.DeleteItem(0)

        sizer.Add(self.list, 1, wx.CENTER)
        # Filter Button elements

        self.btnPrice = wx.Button(panel, label='Price Filter', pos=(15, 55))
        sizer.Add(self.btnPrice, 0, wx.ALL | wx.LEFT, 5)
        self.btnPrice.Bind(wx.EVT_BUTTON, self.OnClicked)

        btnReview = wx.Button(panel, label='Monthly Filter', pos=(15, 60))
        sizer.Add(btnReview, 0, wx.ALL | wx.LEFT, 5)

        btnRoom = wx.Button(panel, label='Room Type Filter', pos=(15, 65))
        sizer.Add(btnRoom, 0, wx.ALL | wx.LEFT, 5)

        btnAvail = wx.Button(panel, label='Availability Filter', pos=(15, 70))
        sizer.Add(btnAvail, 0, wx.ALL | wx.LEFT, 5)

        panel.SetSizer(sizer)
        panel.Fit()

        self.Show()
# Functions

    def OnClicked(self, event):
        btn = event.GetEventObject().GetLabel()
        print(btn)
        if btn == "Price Filter":
            self.button1()
        elif btn == "Monthly Filter":
            self.button2()
        elif btn == "Room Type Filter":
            self.button3()
        elif btn == "Availability Filter":
            self.button4()

# Function for second button (monthly prices)
    def button1(self):
        print("stuff")

# Function for the first button (monthly reviews)
    def button2(self):
        print("stuff")

# Function for third button (room type)
    def button3(self):
        print("stuff")

# Function for forth button (availability)
    def button4(self):
        print("stuff")


if __name__ == '__main__':
    var = "test"
    app = wx.App()
    frame = MyFrame(var)
    app.MainLoop()






